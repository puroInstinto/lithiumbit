## About LithiumBit

<img src="http://storage.lithiumbit.com/storage/logo-LithiumBit-web.png">
Lithiumbit is a cryptocurrency focused on providing a decentralized mechanism of exchange, and anonymity via untraceable and unlinkable transactions.

You can read more about it at http://www.lithiumbit.com

## LithiumBit
# LithiumBit es un proyecto Chileno cuyo fin es facilitar el envío y recepción de dinero entre personas o empresas a través de sus Apps de manera rápida, segura y económica.
# Lanzamiento 28/04/2018


## PARAMETROS

# Algoritmo: CryptoNight
# Pre minado: 1,5%
# Airdrop: 33% del pre minado
# Max Supply: 75.000.000
# Speed factor: 19
# Difficulty target: 180
# Block reward: Variable partiendo en 70 LBIT